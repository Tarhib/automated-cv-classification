# Naive Bayes

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
# Importing the dataset



data_path = "resume_data.csv"

dataset = pd.read_csv(data_path,error_bad_lines=False,encoding = "ISO-8859-1")
row_label_data=[]
label_data=[]

for i in range(14804):
    #print()
    datalabel=str(dataset['Skills'][i])
    row_label_data.append(dataset['Skills'][i])
    if ('C++' in datalabel):
       dataset['label'][i]='C++ and Java Developer' 
    elif ('MYSQl' in datalabel)or('SQl' in datalabel)or('Oracle' in datalabel):
       dataset['label'][i]='Database Admin' 
    elif ('android' in datalabel):
       dataset['label'][i]='Mobile App Developer' 
    elif ('Spring' in datalabel):
       dataset['label'][i]='Spring Developer' 
    elif ('Javascript' in datalabel):
       dataset['label'][i]='Front End Developer'
    elif ('Office' in datalabel)or('MS Excel' in datalabel)or('Powerpoint' in datalabel):
       dataset['label'][i]='Microsoft Office Expert'
    else:
       dataset['label'][i]='Software Developer'
    label_data.append(dataset['label'][i])
    
dataset.to_csv('lable_resume_data.csv', index=False) 

#dataset = pd.read_csv('lable_resume_data.csv',error_bad_lines=False,encoding = "ISO-8859-1")


def cleanHtml(sentence):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, ' ', str(sentence))
    return cleantext


def cleanPunc(sentence): #function to clean the word of any punctuation or special characters
    cleaned = re.sub(r'[?|!|\'|"|#]',r'',sentence)
    cleaned = re.sub(r'[.|,|)|(|\|/]',r' ',cleaned)
    cleaned = cleaned.strip()
    cleaned = cleaned.replace("\n"," ")
    return cleaned


def keepAlpha(sentence):
    alpha_sent = ""
    for word in sentence.split():
        alpha_word = re.sub('[^a-z A-Z]+', ' ', word)
        alpha_sent += alpha_word
        alpha_sent += " "
    alpha_sent = alpha_sent.strip()
    return alpha_sent

dataset['Skills'] = dataset['Skills'].str.lower()
dataset['Skills']= dataset['Skills'].apply(cleanHtml)
dataset['Skills'] = dataset['Skills'].apply(cleanPunc)
dataset['Skills'] = dataset['Skills'].apply(keepAlpha)

from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
def stemming(sentence):
    stemSentence = ""
    for word in sentence.split():
        stem = stemmer.stem(word)
        stemSentence += stem
        stemSentence += " "
    stemSentence = stemSentence.strip()
    return stemSentence

dataset['Skills'] = dataset['Skills'].apply(stemming)


  

#dataset['Educations'] = dataset['Educations'].astype(float)
#dataset['label'] = pd.to_numeric(dataset['label'], errors='coerce')
X = dataset['Skills']
y = dataset['label']








# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)







# Feature Scaling
from sklearn.feature_extraction.text import CountVectorizer
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)

X_test_counts = count_vect.transform(X_test)



############### Naive Bayes Classifier Report ###########


from sklearn.naive_bayes import MultinomialNB

classifier = MultinomialNB().fit(X_train_counts, y_train)


# Predicting the Test set results
y_pred = classifier.predict(X_test_counts)

print("Accuracy Result for Naive Bayes Classifier")
print(classification_report(y_test, classifier.predict(X_test_counts)))



# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
#print(cm)




############### K Neraest Neighbour###########

############################################

from sklearn.neighbors import KNeighborsClassifier
neigh = KNeighborsClassifier(n_neighbors=3)

classifier = neigh.fit(X_train_counts, y_train)


# Predicting the Test set results
y_pred = classifier.predict(X_test_counts)

print("Accuracy Result for K Neraest Neighbour")
print(classification_report(y_test, classifier.predict(X_test_counts)))


# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
#print(cm)


############### Random Forest Classification###########

from sklearn.ensemble import RandomForestClassifier

classifier = DecisionTreeClassifier().fit(X_train_counts, y_train)


# Predicting the Test set results
y_pred = classifier.predict(X_test_counts)

print("Accuracy Result for Decison Tree  Classifier")
print(classification_report(y_test, classifier.predict(X_test_counts)))
# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
#print(cm)


############### Decison Tree  Classifier###########

from sklearn.tree import DecisionTreeClassifier

classifier = DecisionTreeClassifier().fit(X_train_counts, y_train)


# Predicting the Test set results
y_pred = classifier.predict(X_test_counts)

print("Accuracy Result for Decison Tree  Classifier")
print(classification_report(y_test, classifier.predict(X_test_counts)))

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
#print(cm)



